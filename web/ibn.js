
// IMPROVE these functions are hanging in space
	/*
	or, preferably, build up a full table and send that
	*/

function getLayerValues( layerChosen )
{
	switch ( layerChosen )
	{
		case 0 :
		{
			// qwerty layer
			return [
				['||', 'Q', 'W', 'E', 'R', 'T','||',/**/ 'Y', 'U', 'I', 'O', 'P' ],
				['||', 'A', 'S', 'D', 'F', 'G','||',/**/ 'H', 'J', 'K', 'L', '; :' ],
				['||', 'Z', 'X', 'C', 'V', 'B','||',/**/ 'N', 'M', ', <', '. >', '\' "' ],
				// --
				[ '//', 'back', 'del', 'ctrl', 'L_6','//',/**/ 'L_7', 'ctrl', 'ent', 'back', '||' ],
				[ '//', 'alt', '0', 'shif', 'spac','//',/**/ 'spac', 'shif', 'cap', 'alt', '||' ]
			];
		}
		case 1 :
		{
		// maltron layer
			return [
				['||', 'Q', 'P', 'Y', 'C', 'B','||',/**/ 'V', 'M', 'U', 'Z', 'L' ],
				['||', 'A', 'N', 'I', 'S', 'F','||',/**/ 'D', 'T', 'H', 'O', 'R' ],
				['||', '. >', 'J', ', <', 'G ', '\' "','||',/**/ '; :', 'W', 'K', '_', 'X' ],
				// --
				['//', 'cut', 'cpy', 'pst', 'L_6','//',/**/ 'L_7', 'caps', 'ins', '/ ?', '||' ],
				['//', 'ctrl', 'back', 'e E', 'shif','//',/**/ 'entr', 'spac', 'del', 'alt', '||' ]
			];
		}
		case 2 :
		{
		// dvorak layer
			return [
				['||', '; :', ', <', '. >', 'P', 'Y','||',/**/ 'F', 'G', 'C', 'R', 'L' ],
				['||', 'A', 'O', 'E', 'U', 'I','||',/**/ 'D', 'H', 'T', 'N', 'S' ],
				['||', '\' "', 'Q', 'J', 'K', 'X','||',/**/ 'B', 'M', 'W', 'V', 'Z' ],
				// --
				[ '//', 'back', 'del', 'ctrl', 'L_6','//',/**/ 'L_7', 'ctrl', 'ent', 'back', '||' ],
				[ '//', 'alt', '2', 'shif', 'spac','//',/**/ 'spac', 'shif', 'cap', 'alt', '||' ]
			];
		}
		case 3 :
		{
		// colemak layer
			return [
				['||', 'Q', 'W', 'F', 'P', 'G','||',/**/ 'J', 'L', 'U', 'Y', '; :' ],
				['||', 'A', 'R', 'S', 'T', 'D','||',/**/ 'H', 'N', 'E', 'I', 'O' ],
				['||', 'Z', 'X', 'C', 'V', 'B','||',/**/ 'K', 'M', ', <', '. >', '\' "' ],
				// --
				['//', '', '', '', '','//',/**/ '', '', '', '', '||' ],
				['//', '', '3', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 4 :
		{
		// workman layer
			return [
				['||', 'Q', 'D', 'R', 'W', 'B','||',/**/ 'J', 'F', 'U', 'P', '; :' ],
				['||', 'A', 'S', 'H', 'T', 'G','||',/**/ 'Y', 'N', 'E', 'O', 'I' ],
				['||', 'Z', 'X', 'M', 'C', 'V','||',/**/ 'K', 'L', ', <', '. >', '\' "' ],
				// --
				['//', '', '', '', '','//',/**/ '', '', '', '', '||' ],
				['//', '', '4', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 5 :
		{
		// arensito layer
			return [
				['||', 'Q', 'L', ',', 'P', '\' "','||',/**/ ';', 'F', 'U', 'D', 'K' ],
				['||', 'A', 'R', 'E', 'N', 'B','||',/**/ 'G', 'S', 'I', 'T', 'O' ],
				['||', 'Z', 'W', '.', 'H', 'J','||',/**/ 'V', 'C', 'Y', 'M', 'X' ],
				// --
				['//', '', '', '', '','//',/**/ '', '', '', '', '||' ],
				['//', '', '5', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 66 :
		{
		// plum layer
			return [
				['||', 'P', 'L', 'U', 'M', 'tab','||',/**/ 'back', 'C', 'F', 'G', 'Q' ],
				['||', 'R', 'E', 'A', 'D', 'O','||',/**/ 'N', 'T', 'H', 'I', 'S' ],
				['||', 'K', 'J', 'V', 'B', ', <','||',/**/ '. >', 'W', 'X', 'Y', 'Z' ],
				// --
				['//', '', '', '', '','//',/**/ '', '', '', '', '||' ],
				['//', '', '66', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 6 :
		{
			// numbers, arrows
			return [
				['||', ' 9', ' 8', ' 7', ' 6', ' 5','||',/**/ 'F2', 'pDn', '\u2191'/*up*/, '\u21b9'/*tab*/, 'pUp' ],
				['||', ' 4', ' 3', ' 2', ' 1', ' 0','||',/**/ 'home', '\u2190'/*lf*/, '\u2193'/*dn*/, '\u2192'/*rt*/, 'end' ],
				['||', 'undo', 'cut', 'copy', 'paste', 'os','||',/**/ 'D', '_', ',', '-', '.' ],
				// --
				['//', '', '', '', 'L_=6','//',/**/ 'L_7', '', '', '', '||' ],
				['//', '', '6', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 7 :
		{
			// punctuation
			return [
				['||', '#', '@', '&', '.', ';','||',/**/ '_', ',', '|', '^', '%' ],
				['||', '*', '+', '{', '(', ':','||',/**/ '"', ')', '}', '-', '=' ],
				['||', '\\', '?', '<', '[', '$','||',/**/ '~', ']', '>', '!', '/' ],
				// --
				['//', '', '', '', 'L_8','//',/**/ 'L_=7', '', '', '', '||' ],
				['//', '', '7', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 8 :
		{
		// fn, mouse layer
			return [
				['||', 'F6', 'F7', 'F8', 'F9', 'F10','||',/**/ '&', 'mb1', 'mmU', 'mb2', 'mwU' ],
				['||', 'F1', 'F2', 'F3', 'F4', 'F5','||',/**/ '&', 'mmL', 'mmD', 'mmR', 'mwD' ],
				['||', 'F11', 'F12', '`', '&', '&','||',/**/ 'prtSc', '&', 'mwL', 'mwR', 'scLk' ],
				// --
				['//', '', '', '', 'L_=8','//',/**/ 'L_9', '', '', '', '||' ],
				['//', '', '8', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 9 :
		{
		// layer hub layer; maybe layer 0 and I always leap to qwerty?
			return [
				['||', 'L_ma1', '!', 'L_dv2', '!', 'L_cl3','||',/**/ 'L_wk4', '!', 'L_ar5', '!', '!' ],
				['||', '!', '!', '!', '!', '!','||',/**/ '!', '!', '!', '!', '!' ],
				['||', 'L_gmA', '!', 'L_ucB', '!', 'L_npC','||',/**/ '!', '!', '!', '!', '!' ],
				// --
				['//', '', '', '', 'L_=9','//',/**/ 'L_=9', '', '', '', '||' ],
				['//', '', '9', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 10 :
		{
		// "gaming" layer qwert left ; arrows, modifiers, numbers right
			return [
				['||', 'Q', 'W', 'E', 'R', 'T','||',/**/ 'P', 'Y', '\u2191'/*up*/, 'K', '1' ],
				['||', 'A', 'S', 'D', 'F', 'G','||',/**/ 'H', '\u2190'/*lf*/, '\u2193'/*dn*/, '\u2192'/*rt*/, '2' ],
				['||', 'Z', 'X', 'C', 'V', 'B','||',/**/ 'M', '*', '*', '*', '3' ],
				// --
				['//', '', '', '', 'L_=A','//',/**/ 'A', '', '', '', '||' ],
				['//', '', 'A', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 11 :
		{
		// unicode layer
			return [
				['||', '\u00a2'/*cent*/, '\u00bc'/*1/4*/, '\u00bd'/*1/2*/, '\u03a3'/*sum*/, '\u00d8'/*Oslash*/,'||',/**/ '\u250f'/*box ul*/, '\u2533'/*box um*/, '\u2513'/*box ur*/, '\u03bb'/*lambda*/, '\u2018'/*sm'dn*/ ],
				['||', '\u00F1'/*n~*/, '\u00a9'/*&copy*/, '\u00b0'/*degrees*/, '\u00b1'/*+-*/, '\u2b0f'/*arrow up*/,'||',/**/ '\u2523'/*box ml*/, '\u254B'/*box mm*/, '\u252B'/*box mr*/, '\u0394'/*delta*/, '\u2019'/*sm'up*/ ],
				['||', '\u00a1'/*down !*/, '\u00bf'/*down ?*/, '\u00d7'/*mult x*/, '\u00f7'/*div/ */, '\u03c0'/*pi*/,'||',/**/ '\u2517'/*box ll*/, '\u253b'/*bos lm*/, '\u251b'/*box lr*/, '\u201c'/*sm"dn*/, '\u201d'/*sm"up*/ ],
				// --
				['//', '', '', '', 'L_=B','//',/**/ 'B', '', '', '', '||' ],
				['//', '', 'B', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		case 12 :
		{
		// numpad, layer
			return [
				['||', 'n-.', 'n-7', 'n-8', 'n-9', 'n--','||',/**/ 'n-=', '', '', '', '' ],
				['||', 'n-0', 'n-4', 'n-5', 'n-6', 'n-+','||',/**/ 'N-lck', '', '', '', '' ],
				['||', 'n -*', 'n-1', 'n-2', 'n-3', 'n-/','||',/**/ 'n-ent', 'n-lck', '', '', 'insr' ],
				// --
				['//', '', '', '', 'L_=C','//',/**/ 'C', '', '', '', '||' ],
				['//', '', 'C', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
		/*
		case  :
		{
		// 
			return [
				['||', '', '', '', '', '','||',/** / '', '', '', '', '' ],
				['||', '', '', '', '', '','||',/** / '', '', '', '', '' ],
				['||', '', '', '', '', '','||',/** / '', '', '', '', '' ],
				// --
				['//', '', '', '', '','//',/** / '', '', '', '', '||' ],
				['//', '', 'N', '', '','//',/** / '', '', '', '', '||' ]
			];
		}
		*/
		default :
		{
			return [
				['||', '', '', '', '', '','||',/**/ '', '', '', '', '' ],
				['||', '', '', '', '', '','||',/**/ '', '', '', '', '' ],
				['||', '', '-1', '', '', '', '||',/**/ '', '', '', '', '' ],
				// --
				['//', '', '', '', '','//',/**/ '', '', '', '', '||' ],
				['//', '', '', '', '','//',/**/ '', '', '', '', '||' ]
			];
		}
	}
}

function fillMissing( keyboard, layerHadGap, gapCoord )
{
	if ( layerHadGap <= 0 || gapCoord == null
			|| gapCoord.length < 1 )
	{
		return keyboard;
	}
	const ccRowInd = 0, ccColInd = 1;
	var currLayer = getLayerValues( layerHadGap -1 );
	var currCoord;
	var remainingGaps = [];
	for ( var gapInd = 0; gapInd < gapCoord.length; gapInd++ )
	{
		if ( gapCoord[ gapInd ] != null ) // assert len is 2
		{
			currCoord = gapCoord[ gapInd ];
			if ( currLayer[ currCoord[ ccRowInd ] ][ currCoord[ ccColInd ] ] != '' )
			{
				keyboard[ currCoord[ ccRowInd ] ][ currCoord[ ccColInd ] ]
						= currLayer[ currCoord[ ccRowInd ] ][ currCoord[ ccColInd ] ]
				gapCoord[ gapInd ] = null;
			}
			else
			{
				remainingGaps.push( currCoord );
			}
		}
	}
	if ( remainingGaps.length > 0 )
	{
		return fillMissing( keyboard, layerHadGap -1, remainingGaps );
	}
	else
	{
		return keyboard;
	}
}

function assembleKeyboard( layerChosen )
{
	var compressedLayer = getLayerValues( layerChosen );
	var missingCoordinates = [];
	var keyBoard = [];
	for ( var rowInd = 0; rowInd < compressedLayer.length; rowInd++ )
	{
		var keyboardRow = [];
		for ( var colInd = 0; colInd < compressedLayer[ rowInd ].length; colInd++  )
		{
			if ( compressedLayer[ rowInd ][ colInd ] != null
					&& compressedLayer[ rowInd ][ colInd ] != '' )
			{
				keyboardRow.push( compressedLayer[ rowInd ][ colInd ] );
			}
			else
			{
				keyboardRow.push( '' );
				missingCoordinates.push( [ rowInd, colInd ] );
			}
		}
		keyBoard.push( keyboardRow );
	}
	if ( missingCoordinates.length > 0 )
	{
		keyBoard = fillMissing( keyBoard, layerChosen, missingCoordinates );
	}
	return keyBoard;
}

function wrapInTag( typeOfTag, value, cssClass )
{
	// if ( value == null ) // and so on
	var prefix = '<'+typeOfTag +'>';
	if ( cssClass != null )
	{
		prefix = '<'+typeOfTag +' class="'+ cssClass +'">';
	}
	var suffix = '</'+typeOfTag +'>';
	return prefix + value + suffix;
}

function tableRowOf( keyRow )
{
	if ( keyRow == null )
	{
		return '<tr></tr>';
	}
	var htmlRow = '';
	const td = 'td', ccLpad = 'gutter', ccMpad = 'space', ccNorm = 'key',
			lpadFlag = '||', mpadFlag = '//';
	var showing;
	for ( var rowInd = 0; rowInd < keyRow.length; rowInd++ )
	{
		showing = keyRow[ rowInd ];
		if ( showing == lpadFlag )
		{
			htmlRow += wrapInTag( td,  '', ccLpad );
		}
		else if ( showing == mpadFlag )
		{
			htmlRow += wrapInTag( td,  '', ccMpad );
		}
		else
		{
			htmlRow += wrapInTag( td,  showing, ccNorm );
		}
	}
	return wrapInTag( 'tr', htmlRow, null );
}

function changedLayer( layerChosen )
{
	var mri = assembleKeyboard( layerChosen );
	if ( mri == null )
	{
		console.log( 'ibn.cl yay ref error' ); // not IE compliant
		return;
	}
	// cheating "for tonight" with unrolled loop
	var keysAsHtml = tableRowOf( mri[ 0 ] ) +'\n\t<!-- row -->\n';
	keysAsHtml += tableRowOf( mri[ 1 ] ) +'\n\t<!-- row -->\n';
	keysAsHtml += tableRowOf( mri[ 2 ] ) +'\n\t<!-- row -->\n';
	document.getElementById( 'topHalf' ).innerHTML = keysAsHtml;
	keysAsHtml = tableRowOf( mri[ 3 ] ) +'\n\t<!-- row -->\n';
	keysAsHtml += tableRowOf( mri[ 4 ] ) +'\n\t<!-- row -->\n';
	document.getElementById( 'bottomHalf' ).innerHTML = keysAsHtml;
}









