
# Impatient Broth Nenem

Means of previewing different keyboard layouts. Specifically [mitosis](https://flashquark.com/product/gb-mitosis-wireless-split-ergonomic-keyboard-w-acrylic-case/). Reflects the layouts I intend to use. Change these in web/ibn.js-getLayerValues( i ). Use by opening web/page.html and pressing the different radio buttons.

### Name

Same madlibs convention as usual : personality adjective, food component, number.  The number, nenem, used here is __six__ in Balinese.










